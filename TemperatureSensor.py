# With inspiration from: https://pimylifeup.com/raspberry-pi-temperature-sensor/
import os
import glob
import time
from influxdb import InfluxDBClient

# Import request errors
import requests.exceptions

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        # timeStamp = time.time()
        # temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c

# Create influxDB client to acces database
client = InfluxDBClient('server', 80, 'user', 'password', 'database')

# While loop to continously read data from probe and send JSON formatted data structure to database client
while True:
    temp = read_temp
    point = [
         {
             "measurement": "Temperature",
             "tags": {
                 "host": "host"
                  },
             "time": int(time.time()),
             "fields": {
                 "T": read_temp()
                  }
              }
         ]
    # Send
    try:
        client.write_points(point, time_precision='s')
    except requests.exceptions.ConnectionError:
        print("Failed to upload data point, connection lost!")
    time.sleep(5)
